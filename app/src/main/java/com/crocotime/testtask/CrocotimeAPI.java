package com.crocotime.testtask;

import com.google.gson.JsonObject;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;

import retrofit2.http.QueryMap;

public interface CrocotimeAPI {
    @GET("9999f95542454da7ad61d0319120355f")
    Call<JsonObject> auth(@QueryMap Map<String,String> data);


    @POST("9999f95542454da7ad61d0319120355f")
    Call<JsonObject> addTrack(@QueryMap(encoded=true) Map<String, String> data);
}
