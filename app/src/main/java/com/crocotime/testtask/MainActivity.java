package com.crocotime.testtask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private Button mBtnStart;
    private RelativeLayout mBtnStartLayout;
    private Button mBtnCancel;
    private Button mBtnSubmit;
    private RelativeLayout mCommentLayout;
    private TextView mTxtTimer;
    private CrocoTimerTask mCrocoTimerTask;
    private Timer mTimer;
    private EditText mInputComment;
    private long mStartTime;
    private long mEndTime;
    private String mSessionHash;
    private final String USER_LOGIN = "admin@worktest.com";
    private final String USER_PASSWORD = "d345gsf352245";
    private CrocotimeAPI mCrocotimeApi;
    private Call<JsonObject> mAuthCall;
    private Call<JsonObject> mAddTrackCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBtnStart = (Button) findViewById(R.id.btnStart);
        mBtnCancel = (Button) findViewById(R.id.btnCancel);
        mBtnSubmit = (Button) findViewById(R.id.btnSubmit);
        mBtnStartLayout = (RelativeLayout) findViewById(R.id.btnStartLayout);
        mCommentLayout = (RelativeLayout) findViewById(R.id.commentLayout);
        mTxtTimer = (TextView) findViewById(R.id.txtTimer);
        mInputComment = (EditText) findViewById(R.id.inputComment);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.crocotime_url))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mCrocotimeApi = retrofit.create(CrocotimeAPI.class);
    }

    public void btnStartClick(View v) {
        if (mTimer != null) {
            mTimer.cancel();
            mBtnStart.setText(getResources().getText(R.string.btn_start_caption));
            mTimer = null;
            mBtnStartLayout.setVisibility(View.GONE);
            mCommentLayout.setVisibility(View.VISIBLE);
        } else {
            mBtnStartLayout.setVisibility(View.VISIBLE);
            mCommentLayout.setVisibility(View.GONE);
            mStartTime = 0;
            mEndTime = 0;

            Calendar calendar = Calendar.getInstance();
            mStartTime = calendar.getTimeInMillis();
            mEndTime = calendar.getTimeInMillis();
            mTxtTimer.setVisibility(View.VISIBLE);
            mTimer = new Timer();
            mCrocoTimerTask = new CrocoTimerTask();
            mTimer.schedule(mCrocoTimerTask, 0, 1000);
            mBtnStart.setText(getResources().getText(R.string.btn_start_stop_caption));
        }
    }

    public void btnCancelClick(View v) {
        if (mAuthCall != null) {
            mAuthCall.cancel();
        }
        if (mAddTrackCall != null) {
            mAddTrackCall.cancel();
        }
        mCommentLayout.setVisibility(View.GONE);
        mBtnStartLayout.setVisibility(View.VISIBLE);
        mInputComment.getText().clear();
    }

    public void btnSubmitClick(View v) {
        final String comment = mInputComment.getText().toString().trim();
        if (!comment.trim().isEmpty()) {

            mBtnSubmit.setEnabled(false);
            if (mSessionHash == null) {
                mBtnSubmit.setText(getResources().getText(R.string.btn_submit_caption_auth));
                User user = new User(USER_LOGIN, USER_PASSWORD);
                Gson gson = new Gson();
                JsonObject userObj = new JsonObject();
                userObj.add("user", gson.toJsonTree(user));

                Map<String, String> data = new HashMap<>();
                data.put("controller", "LogonController");
                data.put("query", userObj.toString());

                mAuthCall = mCrocotimeApi.auth(data);
                mAuthCall.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        mSessionHash = response.body().get("session").getAsString();
                        Log.d("HASH", mSessionHash + "");
                        if (mSessionHash != null)
                            addTrack(comment);
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Log.e("AUTH_ERR", t.getMessage() + "");
                        Toast.makeText(getApplicationContext(), "Произошла ошибка авторизации", Toast.LENGTH_LONG).show();
                        mBtnSubmit.setText(getResources().getText(R.string.btn_submit_caption));
                        mBtnSubmit.setEnabled(true);
                    }
                });

            } else {
                addTrack(comment);
            }
        } else {
            Toast.makeText(this, "Необходимо указать комментарий", Toast.LENGTH_LONG).show();
        }
    }

    private void addTrack(String comment) {
        mBtnSubmit.setText(getResources().getText(R.string.btn_submit_caption_sending));
        JsonObject params = new JsonObject();
        params.addProperty("task_id", 55);
        params.addProperty("employee_id", 1);
        params.addProperty("begin", (mStartTime + 10800000L) / 1000L);
        params.addProperty("end", (mEndTime + 10800000L) / 1000L);
        params.addProperty("comment", comment);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("action", "insert");
        jsonObject.addProperty("domain", "task_timings");
        jsonObject.add("params", params);

        HashMap data = new HashMap<String, String>();
        data.put("session", mSessionHash);
        data.put("query", jsonObject.toString());
        data.put("controller", "WorkspaceActionController");

        mAddTrackCall = mCrocotimeApi.addTrack(data);
        mAddTrackCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonElement errors = response.body().get("errors");
                if (errors == null) {
                    Toast.makeText(getApplicationContext(), "Трек успешно добавлен", Toast.LENGTH_LONG).show();
                    mCommentLayout.setVisibility(View.GONE);
                    mBtnStartLayout.setVisibility(View.VISIBLE);
                    mInputComment.getText().clear();
                } else {
                    Toast.makeText(getApplicationContext(), "Произошла ошибка при добавлении трека", Toast.LENGTH_LONG).show();
                }

                mBtnSubmit.setText(getResources().getText(R.string.btn_submit_caption));
                mBtnSubmit.setEnabled(true);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e("TRACK_ERR", t.getMessage() + "");
                Toast.makeText(getApplicationContext(), "Произошла ошибка при добавлении трека", Toast.LENGTH_LONG).show();
                mBtnSubmit.setText(getResources().getText(R.string.btn_submit_caption));
                mBtnSubmit.setEnabled(true);
            }
        });
    }

    private class CrocoTimerTask extends TimerTask {

        @Override
        public void run() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
            mEndTime += 1000L;
            final String strDate = simpleDateFormat.format(mEndTime);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTxtTimer.setText(strDate);
                }
            });
        }
    }

}
